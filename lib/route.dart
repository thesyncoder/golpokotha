import 'package:flutter/material.dart';
import 'package:golpokotha/pages/StoryScreen.dart';
// Import other pages


class Router{
  static Route<dynamic> generateRoute( RouteSettings settings){

    switch( settings.name){

      case ( StoryScreen.RouteName):
        final ScreenArguments args = settings.arguments;
        return MaterialPageRoute(
          builder: (context){
            return StoryScreen(
              title: args.title,
              models : args.models,
            );
          }
        );
        break;


      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}')),
            ));
        break;

    }
  }
}