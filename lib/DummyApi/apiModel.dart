class DummyAPi {
  Schema schema;
  List<Data> data;

  DummyAPi({this.schema, this.data});

  DummyAPi.fromJson(Map<String, dynamic> json) {
    schema =
    json['schema'] != null ? new Schema.fromJson(json['schema']) : null;
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.schema != null) {
      data['schema'] = this.schema.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Schema {
  List<Fields> fields;
  List<String> primaryKey;
  String pandasVersion;

  Schema({this.fields, this.primaryKey, this.pandasVersion});

  Schema.fromJson(Map<String, dynamic> json) {
    if (json['fields'] != null) {
      fields = new List<Fields>();
      json['fields'].forEach((v) {
        fields.add(new Fields.fromJson(v));
      });
    }
    primaryKey = json['primaryKey'].cast<String>();
    pandasVersion = json['pandas_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.fields != null) {
      data['fields'] = this.fields.map((v) => v.toJson()).toList();
    }
    data['primaryKey'] = this.primaryKey;
    data['pandas_version'] = this.pandasVersion;
    return data;
  }
}

class Fields {
  String name;
  String type;

  Fields({this.name, this.type});

  Fields.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    return data;
  }
}

class Data {
  int index;
  String storyName;
  String storyAuthorIdS;
  String imageURL;
  int chapters;
  int Duration;
  List<Genre> genre;
  int likeCount;
  int listenCount;
  String publishDateS;
  String date;
  String duration;

  Data(
      {this.index,
        this.storyName,
        this.storyAuthorIdS,
        this.imageURL,
        this.chapters,
        this.Duration,
        this.genre,
        this.likeCount,
        this.listenCount,
        this.publishDateS,
        this.date,
        this.duration});

  Data.fromJson(Map<String, dynamic> json) {
    index = json['index'];
    storyName = json['Story Name'];
    storyAuthorIdS = json['storyAuthorId (S)'];
    imageURL = json['imageURL'];
    chapters = json['chapters'];
    Duration = json['Duration'];
    if (json['Genre'] != null) {
      genre = new List<Genre>();
      json['Genre'].forEach((v) {
        genre.add(new Genre.fromJson(v));
      });
    }
    likeCount = json['likeCount'];
    listenCount = json['ListenCount'];
    publishDateS = json['publishDate (S)'];
    date = json['date'];
    duration = json['duration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['index'] = this.index;
    data['Story Name'] = this.storyName;
    data['storyAuthorId (S)'] = this.storyAuthorIdS;
    data['imageURL'] = this.imageURL;
    data['chapters'] = this.chapters;
    data['Duration'] = this.duration;
    if (this.genre != null) {
      data['Genre'] = this.genre.map((v) => v.toJson()).toList();
    }
    data['likeCount'] = this.likeCount;
    data['ListenCount'] = this.listenCount;
    data['publishDate (S)'] = this.publishDateS;
    data['date'] = this.date;
    data['duration'] = this.duration;
    return data;
  }
}

class Genre {
  String s;

  Genre({this.s});

  Genre.fromJson(Map<String, dynamic> json) {
    s = json['S'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['S'] = this.s;
    return data;
  }
}