const config ='''{
    "schema": {
        "fields": [
            {
                "name": "index",
                "type": "integer"
            },
            {
                "name": "Story Name",
                "type": "string"
            },
            {
                "name": "storyAuthorId (S)",
                "type": "string"
            },
            {
                "name": "imageURL",
                "type": "string"
            },
            {
                "name": "chapters",
                "type": "integer"
            },
            {
                "name": "Duration",
                "type": "integer"
            },
            {
                "name": "Genre",
                "type": "string"
            },
            {
                "name": "likeCount",
                "type": "integer"
            },
            {
                "name": "ListenCount",
                "type": "integer"
            },
            {
                "name": "publishDate (S)",
                "type": "string"
            },
            {
                "name": "date",
                "type": "string"
            },
            {
                "name": "duration",
                "type": "string"
            }
        ],
        "primaryKey": [
            "index"
        ],
        "pandas_version": "0.20.0"
    },
    "data": [
        {
            "index": 0,
            "Story Name": "Story 1",
            "storyAuthorId (S)": "Author 1 ",
            "imageURL": "Assets/image1.jpg",
            "chapters": 2,
            "Duration": 23,
            "Genre": [
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 5,
            "ListenCount": 34,
            "publishDate (S)": "2019-08-01 10:38:21.919Z",
            "date": "2019-08-01",
            "duration": "10:38:21.919"
        },
        {
            "index": 1,
            "Story Name": "Story 2",
            "storyAuthorId (S)": "Author 2",
            "imageURL": "Assets/image2.jpg",
            "chapters": 3,
            "Duration": 60,
            "Genre": [
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 7,
            "ListenCount": 45,
            "publishDate (S)": "2019-08-01 10:38:21.919Z",
            "date": "2019-08-01",
            "duration": "10:38:21.919"
        },
        {
            "index": 2,
            "Story Name": "Story 3",
            "storyAuthorId (S)": "Author 3",
            "imageURL": "Assets/image3.jpg",
            "chapters": 4,
            "Duration": 12,
            "Genre": [
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 5,
            "ListenCount": 23,
            "publishDate (S)": "2019-08-01 10:38:21.919Z",
            "date": "2019-08-01",
            "duration": "10:38:21.919"
        },
        {
            "index": 3,
            "Story Name": "Story 4",
            "storyAuthorId (S)": "Author 4",
            "imageURL": "Assets/image4.jpg",
            "chapters": 1,
            "Duration": 34,
            "Genre": [
                {
                    "S": "Horror"
                },
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 6,
            "ListenCount": 45,
            "publishDate (S)": "2019-08-18 09:38:21.919Z",
            "date": "2019-08-18",
            "duration": "09:38:21.919"
        },
        {
            "index": 4,
            "Story Name": "Story 5",
            "storyAuthorId (S)": "Author 5",
            "imageURL": "Assets/image5.jpg",
            "chapters": 2,
            "Duration": 9,
            "Genre": [
                {
                    "S": "Horror"
                }
            ],
            "likeCount": 3,
            "ListenCount": 0,
            "publishDate (S)": "2019-08-18 09:38:21.919Z",
            "date": "2019-08-18",
            "duration": "09:38:21.919"
        },
        {
            "index": 5,
            "Story Name": "Story 6",
            "storyAuthorId (S)": "Author 6",
            "imageURL": "Assets/image1.jpg",
            "chapters": 3,
            "Duration": 12,
            "Genre": [
                {
                    "S": "Horror"
                },
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 8,
            "ListenCount": 65,
            "publishDate (S)": "2019-08-18 09:38:21.919Z",
            "date": "2019-08-18",
            "duration": "09:38:21.919"
        },
        {
            "index": 6,
            "Story Name": "Story 7",
            "storyAuthorId (S)": "Author 7",
            "imageURL": "Assets/image2.jpg",
            "chapters": 4,
            "Duration": 23,
            "Genre": [
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 9,
            "ListenCount": 65,
            "publishDate (S)": "2019-08-01 10:38:21.919Z",
            "date": "2019-08-01",
            "duration": "10:38:21.919"
        },
        {
            "index": 7,
            "Story Name": "Story 8",
            "storyAuthorId (S)": "Author 8",
            "imageURL": "Assets/image3.jpg",
            "chapters": 5,
            "Duration": 54,
            "Genre": [
                {
                    "S": "Humor"
                },
                {
                    "S": "Children"
                }
            ],
            "likeCount": 23,
            "ListenCount": 78,
            "publishDate (S)": "2019-05-14 09:38:21.919Z",
            "date": "2019-05-14",
            "duration": "09:38:21.919"
        },
        {
            "index": 8,
            "Story Name": "Story 9",
            "storyAuthorId (S)": "Author 9",
            "imageURL": "Assets/image4.jpg",
            "chapters": 6,
            "Duration": 76,
            "Genre": [
                {
                    "S": "Horror"
                },
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 34,
            "ListenCount": 87,
            "publishDate (S)": "2019-08-18 09:38:21.919Z",
            "date": "2019-08-18",
            "duration": "09:38:21.919"
        },
        {
            "index": 9,
            "Story Name": "Story 10",
            "storyAuthorId (S)": "Author 10",
            "imageURL": "Assets/image5.jpg",
            "chapters": 3,
            "Duration": 87,
            "Genre": [
                {
                    "S": "Horror"
                },
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 56,
            "ListenCount": 65,
            "publishDate (S)": "2019-08-18 09:38:21.919Z",
            "date": "2019-08-18",
            "duration": "09:38:21.919"
        },
        {
            "index": 10,
            "Story Name": "Story 11",
            "storyAuthorId (S)": "Author 11",
            "imageURL": "Assets/image1.jpg",
            "chapters": 2,
            "Duration": 32,
            "Genre": [
                {
                    "S": "Horror"
                },
                {
                    "S": "Classic"
                }
            ],
            "likeCount": 7,
            "ListenCount": 66,
            "publishDate (S)": "2019-08-18 09:38:21.919Z",
            "date": "2019-08-18",
            "duration": "09:38:21.919"
        },
        {
            "index": 11,
            "Story Name": "Story 12",
            "storyAuthorId (S)": "Author 12",
            "imageURL": "Assets/image2.jpg",
            "chapters": 1,
            "Duration": 21,
            "Genre": [
                {
                    "S": "Children"
                },
                {
                    "S": "Humor"
                }
            ],
            "likeCount": 7,
            "ListenCount": 54,
            "publishDate (S)": "2019-10-01 09:38:21.919Z",
            "date": "2019-10-01",
            "duration": "09:38:21.919"
        }
    ]
}
''';