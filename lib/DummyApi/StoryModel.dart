import 'package:golpokotha/DummyApi/DummyJson.dart';
import 'package:golpokotha/DummyApi/apiModel.dart';
import 'package:meta/meta.dart';
import 'dart:convert';
class StoryModel{
  final String name;
  final String imagePath;
  final String authorName;
  final int CountUsersListened;
  final int CountUsersLiked;
  final int NoOfChapters;
  final int  duration;
  final String PublicationDate;
  final List<String> Genre;
  StoryModel( {
      @required this.name,
      @required this.imagePath,
      @required this.authorName,
      @required this.CountUsersListened,
      @required this.CountUsersLiked,
      @required this.NoOfChapters,
      @required this.duration,
      @required this.PublicationDate,
      @required this.Genre

  });
}

List<StoryModel> stories =[];
List<StoryModel> NewlyAddedStories =[];
List<StoryModel> PopularStories =[];
List<StoryModel> ChildrenStories =[];
List<StoryModel> HorrorStories =[];
List<StoryModel> ClassicStories =[];


DummyAPi fetch(){
  return DummyAPi.fromJson(jsonDecode(config));
}




void  Fill() {
  final apiResult = fetch();
  final data_list = apiResult.data;
  for (var i = 0; i < data_list.length; i++) {
    List<String> genres = [];
    for (var j = 0; j < data_list[i].genre.length; j++) {
      genres.add(data_list[i].genre[j].s);
    }


    stories.add(StoryModel(
        name: data_list[i].storyName,
        imagePath: data_list[i].imageURL,
        authorName: data_list[i].storyAuthorIdS,
        CountUsersListened: data_list[i].listenCount,
        CountUsersLiked: data_list[i].likeCount,
        NoOfChapters: data_list[i].chapters,
        duration: data_list[i].Duration,
        PublicationDate: data_list[i].publishDateS,
        Genre: genres));
  }



  NewlyAddedStories = stories;

  NewlyAddedStories.sort( (story1 , story2){
    var story1Time = DateTime.parse(story1.PublicationDate);
    var story2Time = DateTime.parse(story2.PublicationDate);
    var  x = story1Time.isBefore(story2Time);
    if ( x){ return 1;}
    else{ return 0;}
  });
  NewlyAddedStories = NewlyAddedStories.sublist(0 , 10);
  print(NewlyAddedStories.length);
//  NewlyAddedStories.forEach((story) => print(story.PublicationDate));

  PopularStories = stories;
  PopularStories.sort( ( story1 , story2){
    var like1 = story1.CountUsersListened;
    var like2 = story2.CountUsersListened;
    if ( like1 <= like2)return 1;
    else return 0;
  });
  PopularStories = PopularStories.sublist(0, 10);
//  PopularStories.forEach((story) => print(story.CountUsersListened));
  ChildrenStories= stories.where( (story)=>
    story.Genre.contains("Children")
  ).toList();
  print(ChildrenStories.length);
  HorrorStories = stories.where( (story)=>
    story.Genre.contains("Horror")
).toList();
  ClassicStories = stories.where( (story)=>
    story.Genre.contains("Classic")
  ).toList();





}









