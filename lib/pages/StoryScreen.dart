import 'dart:math';
import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:golpokotha/DummyApi/StoryModel.dart';
import 'package:golpokotha/styles.dart';

class ScreenArguments{
  final String title;
  final List<StoryModel> models;

  ScreenArguments({
   @required this.title,
    @required this.models,
});
}


class StoryScreen extends StatelessWidget{

  static const RouteName = '/StoryScreen';
  final String title;
  final List<StoryModel> models;

  const StoryScreen({
    Key key,
    @required this.title,
    @required this.models,

}) : super(key : key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text(
          title , style: normalStyle,
        ),
      ),
      body: new Container(
        height: double.infinity,
        child: ListView.separated(
          shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: _makeCard,
          itemCount: models.length,
          padding: EdgeInsets.all(8.0),
          separatorBuilder: ( BuildContext context , int inedex){
            return SizedBox(height: 20.0);
          },
        )
      )
    );

  }

  Widget _makeCard( context ,index) {

     return Padding(
       padding: const EdgeInsets.all(1.0),
       child: Container(
         height: 215.0,
         child: Card(
           elevation: 12.0,
           shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.all(Radius.circular(20.0))
           ),
           child: Column(
             children: <Widget>[
               SizedBox(height: 5.0),
               Padding(
                 padding: const EdgeInsets.all(5.0),
                 child: Row(


                   children: <Widget>[
                     ClipRRect(


                       child: Image.asset(
                         models[index].imagePath,
                         height: 120.0,
                         width: 120.0,

                       ),
                       borderRadius: BorderRadius.circular(20.0),

                     ),
                      Container(
                        height: 140.0,
                        width: 200.0,
                        child: Column(
                           mainAxisAlignment: MainAxisAlignment.start,

                           children: <Widget>[
                             Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                               children: <Widget>[
                                 SizedBox(
                                   width: 8.0
                                 ),
                                 Text(
                                   models[index].name,
                                   maxLines: 2,
                                   style: normalStyle,
                                   textAlign: TextAlign.end,
                                 ),
                                 SizedBox(
                                   width: 70.0
                                 ),
                                 Transform.rotate(
                                   angle: pi/2,
                                   child: Icon(
                                     Icons.linear_scale,
                                     color: Colors.black,
                                     size: 30.0,
                                   ),
                                 )
                               ],
                             ),
                             SizedBox( height:  10.0),

                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               crossAxisAlignment: CrossAxisAlignment.end,

                               children: <Widget>[
                                 SizedBox(
                                   width: 10.0,
                                 ),

                                 Icon(
                                   Icons.library_books,
                                   color: Colors.grey,
                                   size: 20.0,
                                 ),
                                 SizedBox(
                                   width: 10.0
                                 ),

                                 Text(
                                   models[index].authorName,
                                   style : cardTextStyle,

                                 ),
                               ],
                             ),
                             SizedBox( height:  10.0),

                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               crossAxisAlignment: CrossAxisAlignment.end,

                               children: <Widget>[
                                 SizedBox(
                                   width: 10.0,
                                 ),

                                 Icon(
                                   Icons.mic,
                                   color: Colors.grey,
                                   size: 20.0,
                                 ),
                                 SizedBox(
                                     width: 10.0
                                 ),

                                 Text(
                                   'Speaker Name',
                                   style : cardTextStyle,

                                 ),
                               ],
                             ),
                             SizedBox(height: 10.0),
                             Row(
                               mainAxisAlignment: MainAxisAlignment.start,
                               children: <Widget>[
                                 SizedBox(
                                   width: 5.0,
                                 ),
                                 Container(
                                   height: 30.0,
                                   width: 130.0,
                                   alignment: Alignment(-0.9 , 0.0),
                                   child: ListView.separated(
                                      itemCount: models[index].Genre.length,
                                       shrinkWrap: true,
                                       scrollDirection: Axis.horizontal,
                                       separatorBuilder: ( BuildContext context , int ind){
                                        return SizedBox(width: 6.0);
                                       },

                                       itemBuilder: ( BuildContext context,int  ind) =>

                                               Container(
                                       height: 5.0,
                                       width: 60.0,
                                       decoration: BoxDecoration(
                                         shape: BoxShape.rectangle,
                                         color: Colors.blue,

                                       ),
                                       child:Center(
                                         child: Text(
                                           models[index].Genre[ind] , style: TextStyle( fontWeight: FontWeight.w600 , color: Colors.white ),
                                         ),
                                       )
                                   ),

                                   ),
                                 ),
                                 ClipRRect(
                                   borderRadius: BorderRadius.circular(8.0),
                                   child: Container(
                                     height: 30.0,
                                     width: 60.0,
                                     decoration: BoxDecoration(
                                       shape: BoxShape.rectangle,
                                       color: Colors.red,
                                     ),
                                     child: Row(
                                       children: <Widget>[
                                         Icon(
                                           Icons.library_books,
                                           size: 20.0,
                                           color: Colors.white,
                                         ),
                                         Text(
                                           '${models[index].NoOfChapters.toString()} chap',
                                           maxLines: 2,
                                           style: TextStyle( fontSize: 12.0 , color: Colors.white),
                                         )
                                       ],
                                     ),
                                   ),
                                 )
                               ],
                             )

                           ],
                         ),
                      ),

                   ],
                 ),
               ),
               SizedBox(
                 height: 1.0,
               ),
               ClipRRect(
                 borderRadius: BorderRadius.circular(20.0),
                 child: Container(
                   height: 50.0,
                   width: double.infinity,
                   decoration: BoxDecoration(

                     color: Colors.grey[300],
                   ),
                   child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.headset,
                                  color: Colors.grey,
                                  size: 30.0,
                                ),
                                Text('${models[index].CountUsersListened} times' , style: cardTextStyle,),

                              ],

                            ),
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.favorite_border,
                                  color: Colors.grey,
                                  size: 30.0,
                                ),
                                Text('${models[index].CountUsersLiked} likes' , style: cardTextStyle,),

                              ],

                            ),
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.timer,
                                  color: Colors.black,
                                  size: 30.0,
                                ),
                                Text('${models[index].duration}', style: cardTextStyle,),

                              ],

                            ),

                          ],
                   ),
                 ),
               )
             ],


           )

         ),
       ),
     );
  }
}