import 'package:flutter/material.dart';

TextStyle appBarTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 24.0,

);

TextStyle normalStyle = TextStyle(
  color: Colors.black87,
  fontSize: 20.0,
  fontWeight: FontWeight.w900,

);

TextStyle cardTextStyle =TextStyle(
  color: Colors.black54,
  fontSize: 15.0,
  fontStyle: FontStyle.italic,
  fontWeight: FontWeight.w500,
);

TextStyle dateTextStyle =TextStyle(
  color: Colors.black54,
  fontSize: 19.0,
  fontStyle: FontStyle.italic,
  fontWeight: FontWeight.w600,
);