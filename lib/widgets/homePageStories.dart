

import 'package:flutter/cupertino.dart';
import 'package:golpokotha/DummyApi/StoryModel.dart';
import 'package:flutter/material.dart';
import 'package:golpokotha/pages/StoryScreen.dart';
import 'package:golpokotha/widgets/Popular.dart';
import 'package:golpokotha/widgets/children.dart';
import 'package:golpokotha/widgets/classic.dart';
import 'package:golpokotha/widgets/horror.dart';

import '../styles.dart';

class Dissplay extends StatefulWidget{
  @override
  DisplayState createState() => DisplayState();
}

class DisplayState extends State<Dissplay>{

  @override
  Widget build(BuildContext context) {
    return
       Padding(
         padding: const EdgeInsets.all(10.0),
         child: ListView(
           scrollDirection: Axis.vertical,
           shrinkWrap: true,
           children: <Widget>[
             Container(
               height: 290.0,
               width : double.infinity,
               child: ListView(
                 shrinkWrap: true,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      Navigator.pushNamed(context, StoryScreen.RouteName ,
                          arguments: ScreenArguments(
                            title: 'Newly Added',
                            models: NewlyAddedStories
                          )
                      );
                    },
                    child: Text(
                      'Newly Added >>',
                      style: normalStyle,
                    ),
                  ),
                  SizedBox(height: 2.0),
              Container(
                height: 260.0,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: _buildNewly,
                  itemCount: NewlyAddedStories.length,
                  scrollDirection: Axis.horizontal,

                ),
              )


                ],

    ),
             ),
             SizedBox(height: 20.0),
             Container(
               height: 290.0,
               width : double.infinity,
               child: Column(
                 children: <Widget>[
                   GestureDetector(
                     onTap: (){
                       Navigator.pushNamed(context, StoryScreen.RouteName ,
                           arguments: ScreenArguments(
                               title: 'Most Popular',
                               models: PopularStories,
                           )
                       );
                     },
                     child: Text(
                       'Most Popular >>',
                       style: normalStyle,
                     ),
                   ),
                   SizedBox(height: 2.0),
                   Container(
                     height: 260.0,
                     child: ListView.builder(
                       shrinkWrap: true,
                       itemBuilder: buildPopular,
                       itemCount: NewlyAddedStories.length,
                       scrollDirection: Axis.horizontal,

                     ),
                   )


                 ],

               ),
             ),
             SizedBox(height: 20.0),
             Container(
               height: 290.0,
               width : double.infinity,
               child: Column(
                 children: <Widget>[
                   GestureDetector(
                     onTap: (){
                       Navigator.pushNamed(context, StoryScreen.RouteName ,
                           arguments: ScreenArguments(
                               title: 'Children',
                               models: ChildrenStories,
                           )
                       );
                     },
                     child: Text(
                       'Children >>',
                       style: normalStyle,
                     ),
                   ),
                   SizedBox(height: 2.0),
                   Container(
                     height: 260.0,
                     child: ListView.builder(
                       shrinkWrap: true,
                       itemBuilder: buildChildren,
                       itemCount: ChildrenStories.length,
                       scrollDirection: Axis.horizontal,

                     ),
                   )


                 ],

               ),
             ),
             SizedBox(height: 20.0),
             Container(
               height: 290.0,
               width : double.infinity,
               child: Column(
                 children: <Widget>[
                   GestureDetector(
                     onTap: (){
                       Navigator.pushNamed(context, StoryScreen.RouteName ,
                           arguments: ScreenArguments(
                               title: 'Horror Stories',
                               models: HorrorStories,
                           )
                       );
                     },
                     child: Text(
                       'Horror >>',
                       style: normalStyle,
                     ),
                   ),
                   SizedBox(height: 2.0),
                   Container(
                     height: 260.0,
                     child: ListView.builder(
                       shrinkWrap: true,
                       itemBuilder: buildHorror,
                       itemCount: HorrorStories.length,
                       scrollDirection: Axis.horizontal,

                     ),
                   )


                 ],

               ),
             ),
             SizedBox(height: 20.0),
             Container(
               height: 290.0,
               width : double.infinity,
               child: Column(
                 children: <Widget>[
                   GestureDetector(
                     onTap: (){
                       Navigator.pushNamed(context, StoryScreen.RouteName ,
                           arguments: ScreenArguments(
                               title: 'Classic Stories',
                               models: ClassicStories,
                           )
                       );
                     },
                     child: Text(
                       'Classic >>',
                       style: normalStyle,
                     ),
                   ),
                   SizedBox(height: 2.0),
                   Container(
                     height: 260.0,
                     child: ListView.builder(
                       shrinkWrap: true,
                       itemBuilder: buildClassic,
                       itemCount: ClassicStories.length,
                       scrollDirection: Axis.horizontal,

                     ),
                   )


                 ],

               ),
             ),
           ],
         ),
       );

  }


  Widget _buildNewly( context , index){
    return Card(
        elevation: 6.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0))
        ),
        child:Container(
          height: 170.0,
          width: 170.0,
          child: Column(
            children: <Widget>[
              SizedBox(height: 5.0),
              Image.asset(
                  '${NewlyAddedStories[index].imagePath}',
                height: 160.0,
                width: 170.0,
                fit: BoxFit.fill,


              ),
              SizedBox(height: 4.0),
              Text(
                NewlyAddedStories[index].name,
                style: cardTextStyle,
              ),
              SizedBox(height: 2.0),
              Text(
                NewlyAddedStories[index].authorName,
                style: cardTextStyle,
              ),
              SizedBox(height: 3.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 60.0,
                    child: Text(
                      '${NewlyAddedStories[index].CountUsersLiked} Likes',
                      style: cardTextStyle,
                    ),
                  ),
                  Container(
                    height: 30.0,
                    width: 60.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Icon(
                          Icons.timer
                        ),
                        Container(
                          height: 15.0,
                          width: 30.0,
                          child: Text(
                            '${(NewlyAddedStories[index].duration/60).round().toString()}:${(NewlyAddedStories[index].duration%60).round().toString()}',
                                style: cardTextStyle,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),



            ],
          ),

        )
    );

  }




}