import 'package:flutter/material.dart';
import 'package:golpokotha/DummyApi/StoryModel.dart';
import 'package:golpokotha/styles.dart';

Widget buildPopular( context , index){
  return Card(
      elevation: 6.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16.0))
      ),
      child:Container(
        height: 170.0,
        width: 170.0,
        child: Column(
          children: <Widget>[
            SizedBox(height: 5.0),
            Image.asset(
              '${PopularStories[index].imagePath}',
              height: 160.0,
              width: 170.0,
              fit: BoxFit.fill,


            ),
            SizedBox(height: 4.0),
            Text(
              PopularStories[index].name,
              style: cardTextStyle,
            ),
            SizedBox(height: 2.0),
            Text(
              PopularStories[index].authorName,
              style: cardTextStyle,
            ),
            SizedBox(height: 3.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 60.0,
                  child: Text(
                    '${PopularStories[index].CountUsersLiked} Likes',
                    style: cardTextStyle,
                  ),
                ),
                Container(
                  height: 30.0,
                  width: 60.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(
                          Icons.timer
                      ),
                      Container(
                        height: 15.0,
                        width: 30.0,
                        child: Text(
                          '${(PopularStories[index].duration/60).round().toString()}:${(PopularStories[index].duration%60).round().toString()}',
                          style: cardTextStyle,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),



          ],
        ),

      )
  );

}